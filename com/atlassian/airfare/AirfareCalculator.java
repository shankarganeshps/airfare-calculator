package com.atlassian.airfare;

public class AirfareCalculator {

	// flat fees and taxes
	final float customsFee          = 5.5f;
	final float immigrationFee      = 7f;
	final float federalTransportTax = .025f;

	public float calculateAirfare(float baseFare) {
	    float fare = baseFare;
	    fare += immigrationFee + customsFee;
	    fare *= federalTransportTax;
	    return fare;
	}

}
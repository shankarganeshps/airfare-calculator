package com.atlassian.airfare;

public class Main {

	public static void main(String[] args) {
		float baseFare = Float.parseFloat(args[0]);
		AirfareCalculator calc = new AirfareCalculator();
		System.out.println("$" + calc.calculateAirfare(baseFare));
	}

}
